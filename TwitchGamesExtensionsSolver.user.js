// ==UserScript==
// @name        Twitch "Games Extensions" Solver
// @namespace   works.kasai.userscripts.twitchgamesextensionssolver
// @version     1.1.7
// @author      Kasai. (kasai.moe)
// @description Twitch "Games Extensions" Solver
// @homepage    https://userscripts.kasai.works#twitchgamesextensionssolver
// @icon        https://www.google.com/s2/favicons?domain=twitch.tv
// @updateURL   https://gitlab.com/kd-works/userscripts/raw/main/TwitchGamesExtensionsSolver.user.js
// @downloadURL https://gitlab.com/kd-works/userscripts/raw/main/TwitchGamesExtensionsSolver.user.js
// @supportURL  https://gitlab.com/kd-works/userscripts/issues/new
// @match       http*://*.twitch.tv/popout/*/extensions/*
// @match       http*://*.twitch.tv/popout/*/extensions/*/*
// @run-at      document-body
// @grant       none
// @noframes
// ==/UserScript==

(() => {
  'use strict'

  class TwitchGamesExtensions {
    GemMiner() {
      let interval = 10

      const Mine = setInterval(() => {
        document.querySelector('.mine-button').click()
      }, interval)

      const StorageSilo = setInterval(() => {
        const els = [...document.querySelectorAll('.upgrade-silo-button')].filter(el => ![...el.classList].includes('is-locked')).filter(el => ![...el.classList].includes('is-empty'))
        els.forEach(button => {
          button.click()
        })
      }, interval)

      const Transport = setInterval(() => {
        const els = [...document.querySelectorAll('.upgrade-capsule-button')].filter(el => ![...el.classList].includes('is-locked')).filter(el => ![...el.classList].includes('is-empty'))
        els.forEach(button => {
          button.click()
        })
      }, interval)

      const Upgrades = setInterval(() => {
        const els = [...document.querySelectorAll('.upgrade-global-button')].filter(el => ![...el.classList].includes('is-maxed'))
        els.forEach(button => {
          button.click()
        })
      }, interval)
    }
  }

  class TwitchGamesExtensionsSolver {
    constructor() {
      this.games = new TwitchGamesExtensions()

      this.GameFinder()
    }

    GameFinder() {
      this.games.GemMiner()
    }
  }

  new TwitchGamesExtensionsSolver()
})()
