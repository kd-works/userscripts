// ==UserScript==
// @name        Google "Easter Eggs" Solver
// @namespace   works.kasai.userscripts.googleeastereggssolver
// @version     1.0.8
// @author      Kasai. (kasai.moe)
// @description Google "Easter Eggs" Solver
// @homepage    https://userscripts.kasai.works#googleeastereggssolver
// @icon        https://www.google.com/favicon.ico
// @updateURL   https://gitlab.com/kd-works/userscripts/raw/main/GoogleEasterEggsSolver.user.js
// @downloadURL https://gitlab.com/kd-works/userscripts/raw/main/GoogleEasterEggsSolver.user.js
// @supportURL  https://gitlab.com/kd-works/userscripts/issues/new
// @include     http*://*.google.*
// @include     http*://*.google.*/*
// @include     http*://elgoog.im
// @include     http*://elgoog.im/*
// @run-at      document-body
// @grant       GM_addStyle
// @noframes
// ==/UserScript==

(() => {
  'use strict'

  class GoogleGames {
    ZergRush() {
      const killZergling = el => [1, 2, 3].forEach(_ => el.dispatchEvent(new MouseEvent('mousedown')))

      document.querySelectorAll('.zr_zergling_container').forEach(killZergling)

      const observer = new MutationObserver(mutationsList => {
        for (const mutation of mutationsList) {
          if (mutation.addedNodes.length > 0) {
            mutation.addedNodes.forEach(killZergling)
          }
        }
      })

      observer.observe(document.body, { childList: true })
    }

    Dino() {
      const Runner = window.Runner

      setInterval(() => {
        const tRex = Runner.instance_.tRex
        const obstacles = Runner.instance_.horizon.obstacles

        if (!tRex.jumping && (obstacles.length > 0) && (obstacles[0].xPos + obstacles[0].width) <= ((parseInt(Runner.instance_.currentSpeed - 0.1) - 5) * 34 + 160) && (obstacles[0].xPos + obstacles[0].width) > 20) {
          tRex.startJump()
        }
      }, 2)
    }
  }

  class GoogleGamesKiller {
    constructor () {
      this.search = document.location.pathname
      this.games = new GoogleGames()

      this.GameFinder()
    }

    GameFinder() {
      if (this.search.includes('zergrush')) this.games.ZergRush()
      // if (this.search.includes('t-rex')) this.games.Dino() // Bugged
    }
  }

  new GoogleGamesKiller()
})()
