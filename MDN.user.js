// ==UserScript==
// @name        Mozilla Developper Network
// @namespace   works.kasai.userscripts.mdn
// @version     1.0.8
// @author      Kasai. (kasai.moe)
// @description Hide useless element
// @homepage    https://userscripts.kasai.works#mdn
// @icon        https://developer.mozilla.org/favicon.ico
// @updateURL   https://gitlab.com/kd-works/userscripts/raw/main/MDN.user.js
// @downloadURL https://gitlab.com/kd-works/userscripts/raw/main/MDN.user.js
// @supportURL  https://gitlab.com/kd-works/userscripts/issues/new
// @match       http*://developer.mozilla.org
// @match       http*://developer.mozilla.org/*
// @run-at      document-body
// @grant       GM_addStyle
// @noframes
// ==/UserScript==

(() => {
  'use strict'

  GM_addStyle('#main-q{outline:none}')
})()
