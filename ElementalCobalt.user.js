// ==UserScript==
// @name        ElementalCobalt
// @namespace   works.kasai.userscripts.elementalcobalt
// @version     2.6.6
// @author      Kasai. (kasai.moe)
// @description Hide useless items (ads or other annoying things)
// @homepage    https://userscripts.kasai.works#elementalcobalt
// @icon        https://elementalcobalt.wordpress.com/favicon.ico
// @updateURL   https://gitlab.com/kd-works/userscripts/raw/main/ElementalCobalt.user.js
// @downloadURL https://gitlab.com/kd-works/userscripts/raw/main/ElementalCobalt.user.js
// @supportURL  https://gitlab.com/kd-works/userscripts/issues/new
// @match       http*://elementalcobalt.wordpress.com
// @match       http*://elementalcobalt.wordpress.com/*
// @run-at      document-start
// @grant       GM_addStyle
// @noframes
// ==/UserScript==

(() => {
  'use strict'

  GM_addStyle('#jp-post-flair,.post-navigation,#comments,#colophon,#actionbar,#masthead{display:none!important}')
})()
