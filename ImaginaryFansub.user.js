// ==UserScript==
// @name        Imaginary Fansub -「Ara Waq`Barh」
// @namespace   works.kasai.userscripts.imaginaryfansub
// @version     1.3.4
// @author      Kasai.
// @description Display a "Ara Waq`Bar" video on the website.
// @homepage    https://userscripts.kasai.works#imaginaryfansub
// @icon        https://www.google.com/s2/favicons?domain=www.imaginary-fansub.com
// @updateURL   https://gitlab.com/kd-works/userscripts/raw/main/ImaginaryFansub.user.js
// @downloadURL https://gitlab.com/kd-works/userscripts/raw/main/ImaginaryFansub.user.js
// @supportURL  https://gitlab.com/kd-works/userscripts/issues/new
// @match       http*://imaginary-fansub.fr.nf
// @match       http*://imaginary-fansub.fr.nf/*
// @match       http*://www.imaginary-fansub.com
// @match       http*://www.imaginary-fansub.com/*
// @run-at      document-body
// @noframes
// ==/UserScript==

(() => {
  'use strict'

  document.getElementById('header').innerHTML = '<aside id="text-3" class="widget widget_text"><h3 class="widget-title">POWERED BY KASAI.</h3><div class="textwidget"><video autoplay><source src="https://u.pomf.is/gpcnrp.webm" type="video/webm"></video></div></aside>'
})()
