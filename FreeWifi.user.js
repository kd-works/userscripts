// ==UserScript==
// @name        Free - FreeWifi
// @namespace   works.kasai.userscripts.freewifi
// @version     1.8.13
// @author      Kasai. (kasai.moe)
// @description Resize the height of the "#bod" tag for responsive devices.
// @homepage    https://userscripts.kasai.works#freewifi
// @icon        https://wifi.free.fr/favicon.ico
// @updateURL   https://gitlab.com/kd-works/userscripts/raw/main/FreeWifi.user.js
// @downloadURL https://gitlab.com/kd-works/userscripts/raw/main/FreeWifi.user.js
// @supportURL  https://gitlab.com/kd-works/userscripts/issues/new
// @match       http*://wifi.free.fr
// @match       http*://wifi.free.fr/*
// @run-at      document-start
// @grant       GM_addStyle
// @noframes
// ==/UserScript==

(() => {
  'use strict'

  GM_addStyle('#bod{height:0px!important}a[href*="$PRIV_SUB"]{display:none!important}')
})()
