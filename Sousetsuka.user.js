// ==UserScript==
// @name        Sousetsuka
// @namespace   works.kasai.userscripts.sousetsuka
// @version     2.5.7
// @author      Kasai. (kasai.moe)
// @description Reset the fonts used by "Indie Flower", "Ubuntu" & "Dosis"
// @homepage    https://userscripts.kasai.works#sousetsuka
// @icon        https://www.sousetsuka.com/favicon.ico
// @updateURL   https://gitlab.com/kd-works/userscripts/raw/main/Sousetsuka.user.js
// @downloadURL https://gitlab.com/kd-works/userscripts/raw/main/Sousetsuka.user.js
// @supportURL  https://gitlab.com/kd-works/userscripts/issues/new
// @match       http*://*.sousetsuka.com
// @match       http*://*.sousetsuka.com/*
// @run-at      document-body
// @grant       GM_addStyle
// @noframes
// ==/UserScript==

(() => {
  'use strict'

  GM_addStyle('@import "https://fonts.googleapis.com/css?family=Indie+Flower|Ubuntu|Dosis";#content-top,#HTML1,#HTML2,#HTML3,#HTML4,#HTML6,#HTML7,#BlogArchive1,#Label1,#Subscribe1,#comments,#blog-pager,#postFooterGadgets,#FollowByEmail1{display:none!important}#content-top,#HTML1,#HTML2,#HTML3,#HTML4,#HTML6,#HTML7,#BlogArchive1,#Label1,#Subscribe1,#comments,#blog-pager,#postFooterGadgets{display:none!important}.post-title{font-family:"Indie Flower",cursive!important}.postmeta{font-family:"Dosis",sans-serif!important}.post-body{font-family:"Ubuntu",sans-serif!important}#credit a{color:rgb(113,113,113)!important}')
})()
