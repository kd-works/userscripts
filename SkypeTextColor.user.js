// ==UserScript==
// @name        Microsoft Skype - Color
// @namespace   works.kasai.userscripts.skypetextcolor
// @version     4.6.4
// @author      Kasai. (kasai.moe)
// @description Add coloring to the text writed.
// @homepage    https://userscripts.kasai.works#skypetextcolor
// @icon        https://www.google.com/s2/favicons?domain=web.skype.com
// @updateURL   https://gitlab.com/kd-works/userscripts/raw/main/SkypeTextColor.user.js
// @downloadURL https://gitlab.com/kd-works/userscripts/raw/main/SkypeTextColor.user.js
// @supportURL  https://gitlab.com/kd-works/userscripts/issues/new
// @match       http*://web.skype.com
// @match       http*://web.skype.com/*
// @run-at      document-body
// @grant       none
// @noframes
// ==/UserScript==

(XHR => {
	'use strict'

	let stats = []
	let timeoutId = null
	let open = XHR.prototype.open
	let send = XHR.prototype.send

	XHR.prototype.open = (method, url, async, user, pass) => {
		this._url = url
		open.call(this, method, url, async, user, pass)
	}

	XHR.prototype.send = data => {
		let k
		let self = this
		let start
		let oldOnReadyStateChange
		let url = this._url

		if (isMsg(data) && data != null) {
			k = JSON.parse(data)
			let size = null

			if (k.content.indexOf("!") > -1) {
				size = k.content.match(/\!(.*?)\!/)[1]
			}

			if (k.content.substring(0, 1) === '#'){
				k = replaceURL(k)

				if (k !== null)
					data = JSON.stringify(k)

				if (size !== null) {
					k = changeFontSize(JSON.parse(data), size)

					if (k !== null) {
						k = JSON.stringify(k)
						data = k
					}
				}
			}

			console.log(data)
		}

		let onReadyStateChange = () => {
			if (self.readyState == 4) {
				let time = new Date() - start

				stats.push({
					url:url,
					duration:time
				})

				if (!timeoutId) {
					timeoutId = window.setTimeout(() => {
						let xhr = new XHR()

						xhr.noIntercept = true
						xhr.open('POST', '/clientAjaxStats', true)
						xhr.setRequestHeader('Content-type', 'application/json')
						xhr.send(JSON.stringify({stats: stats}))
						timeoutId = null
						stats = []
					},2000)
				}
			}

			if (oldOnReadyStateChange) {
				oldOnReadyStateChange()
			}
		}

		if (!this.noIntercept) {
			start = new Date()

			if (this.addEventListener) {
				this.addEventListener('readystatechange', onReadyStateChange, false)
			} else {
				oldOnReadyStateChange = this.onreadystatechange
				this.onreadystatechange = onReadyStateChange
			}
		}

		send.call(this, data)
	}
})(XMLHttpRequest)

let isMsg = value => {
	try {
		JSON.stringify(value)

		if (JSON.stringify(value).indexOf('content') > -1)
			return true
		else
			return false
	}
	catch (ex) {
		return false
    }
}

let replaceURL = data => {
	try {
		if (data.content.indexOf('<a href="') > -1) {
			let remove = data.content.match(/\<(.*?)\>/)[1]

			data.content = data.content.replace(remove, '')
			data.content = data.content.replace('<', '')
			data.content = data.content.replace('>', '')
			data.content = data.content.replace('</a>', '')

			console.log(data.content)
		}

		if (data.content.match(/\[(.*?)\]/)[1].indexOf('http://') == -1 && data.content.match(/\[(.*?)\]/)[1].indexOf('https://') == -1)
			data.content = `<a href="http://${data.content.match(/\[(.*?)\]/)[1]}">${data.content.match(/\(([^)]+)\)/)[1]}</a>`
		else
			data.content = `<a href="${data.content.match(/\[(.*?)\]/)[1]}">${data.content.match(/\(([^)]+)\)/)[1]}</a>`

		return data
	}
	catch (ex) {
		return null
    }
}

let changeFontSize = (data, size) => {
	try {
		if (data.content.indexOf('#') > -1) {
			data.content = data.content.replace('#', '');
			data.content = data.content.replace(data.content.match(/\!(.*?)\!/)[1], '');
			data.content = data.content.replace(/!/g, '');
		}

		data.content = `<font color="${size}">${data.content}</font>`
		return data
	}
	catch (ex) {
		return null
    }
}
