// ==UserScript==
// @name        GitHub
// @namespace   works.kasai.userscripts.github
// @version     1.0.3
// @author      Kasai. (kasai.moe)
// @description Remove flash "warn" notice from Github
// @homepage    https://userscripts.kasai.works#github
// @icon        https://github.githubassets.com/favicons/favicon.png
// @updateURL   https://gitlab.com/kd-works/userscripts/raw/main/GitHub.user.js
// @downloadURL https://gitlab.com/kd-works/userscripts/raw/main/GitHub.user.js
// @supportURL  https://gitlab.com/kd-works/userscripts/issues/new
// @match       http*://github.com
// @match       http*://github.com/*
// @run-at      document-body
// @grant       GM_addStyle
// @noframes
// ==/UserScript==

(() => {
  'use strict'

  GM_addStyle('body .flash.flash-full.js-notice.flash-warn{display:none!important}')
})()
