// ==UserScript==
// @name        MS - TechBench
// @namespace   works.kasai.userscripts.mstechbench
// @version     2.7.5
// @author      Kasai. (kasai.moe)
// @description Add many options to the select area for more choice when it comes to download a windows version.
// @homepage    https://userscripts.kasai.works#mstechbench
// @icon        https://www.microsoft.com/favicon.ico
// @updateURL   https://gitlab.com/kd-works/userscripts/raw/main/MSTechBench.user.js
// @downloadURL https://gitlab.com/kd-works/userscripts/raw/main/MSTechBench.user.js
// @supportURL  https://gitlab.com/kd-works/userscripts/issues/new
// @match       http*://www.microsoft.com/*/software-download/windows10ISO
// @run-at      document-body
// @grant       none
// @noframes
// ==/UserScript==

(() => {
  'use strict'

  let html

  const templates = {
    select: '<option value="" selected="selected">Select edition</option>',
    optgroup: '<optgroup label="%label%">%list%</optgroup>',
    option: '<option value="%value%" %disabled%>%name%</option>'
  }

  const editions = [{
    name: 'Windows 7', // Deprecated
    products: [{
      id: 1,
      disabled: true // doesn't work anymore
    }, {
      id: 2,
      name: 'Windows 7 Home Basic SP1',
      disabled: true // doesn't work anymore
    }, {
      id: 3,
      disabled: true // doesn't work anymore
    }, {
      id: 4,
      name: 'Windows 7 Professional SP1',
      disabled: true // doesn't work anymore
    }, {
      id: 5,
      disabled: true // doesn't work anymore
    }, {
      id: 6,
      name: 'Windows 7 Home Premium SP1',
      disabled: true // doesn't work anymore
    }, {
      id: 7,
      disabled: true // doesn't work anymore
    }, {
      id: 8,
      name: 'Windows 7 Ultimate SP1',
      disabled: true // doesn't work anymore
    }, {
      id: 9,
      disabled: true // doesn't work anymore
    }, {
      id: 10,
      name: 'Windows 7 Home Premium N SP1',
      disabled: true // doesn't work anymore
    }, {
      id: 11,
      disabled: true // doesn't work anymore
    }, {
      id: 12,
      name: 'Windows 7 Professional N SP1',
      disabled: true // doesn't work anymore
    }, {
      id: 13,
      disabled: true // doesn't work anymore
    }, {
      id: 14,
      name: 'Windows 7 Ultimate N SP1',
      disabled: true // doesn't work anymore
    }, {
      id: 15,
      disabled: true // doesn't work anymore
    }, {
      id: 16,
      name: 'Windows 7 Professional K SP1',
      disabled: true // doesn't work anymore
    }, {
      id: 17,
      disabled: true // doesn't work anymore
    }, {
      id: 18,
      name: 'Windows 7 Professional KN SP1',
      disabled: true // doesn't work anymore
    }, {
      id: 19,
      disabled: true // doesn't work anymore
    }, {
      id: 20,
      name: 'Windows 7 Home Premium K SP1',
      disabled: true // doesn't work anymore
    }, {
      id: 21,
      disabled: true // doesn't work anymore
    }, {
      id: 22,
      name: 'Windows 7 Home Premium KN SP1',
      disabled: true // doesn't work anymore
    }, {
      id: 23,
      disabled: true // doesn't work anymore
    }, {
      id: 24,
      name: 'Windows 7 Ultimate KN SP1',
      disabled: true // doesn't work anymore
    }, {
      id: 25,
      disabled: true // doesn't work anymore
    }, {
      id: 26,
      name: 'Windows 7 Ultimate K SP1',
      disabled: true // doesn't work anymore
    }, {
      id: 27,
      disabled: true // doesn't work anymore
    }, {
      id: 28,
      name: 'Windows 7 Starter SP1',
      disabled: true // doesn't work anymore
    }, {
      id: 29,
      disabled: true // doesn't work anymore
    }, {
      id: 30,
      disabled: true // doesn't work anymore
    }, {
      id: 31,
      disabled: true // doesn't work anymore
    }, {
      id: 32,
      disabled: true // doesn't work anymore
    }, {
      id: 33,
      disabled: true // doesn't work anymore
    }, {
      id: 34,
      disabled: true // doesn't work anymore
    }, {
      id: 35,
      disabled: true // doesn't work anymore
    }, {
      id: 36,
      disabled: true // doesn't work anymore
    }, {
      id: 37,
      disabled: true // doesn't work anymore
    }, {
      id: 38,
      disabled: true // doesn't work anymore
    }, {
      id: 39,
      disabled: true // doesn't work anymore
    }, {
      id: 40,
      disabled: true // doesn't work anymore
    }, {
      id: 83,
      name: 'Windows 7 Home Basic SP1 COEM',
      disabled: true // doesn't work anymore
    }, {
      id: 85,
      name: 'Windows 7 Home Basic SP1 COEM GGK',
      disabled: true // doesn't work anymore
    }, {
      id: 86,
      name: 'Windows 7 Home Premium N SP1 COEM',
      disabled: true // doesn't work anymore
    }, {
      id: 87,
      name: 'Windows 7 Home Premium SP1 COEM',
      disabled: true // doesn't work anymore
    }, {
      id: 88,
      name: 'Windows 7 Home Premium SP1 COEM GGK',
      disabled: true // doesn't work anymore
    }, {
      id: 89,
      name: 'Windows 7 Home Premium K SP1 COEM',
      disabled: true // doesn't work anymore
    }, {
      id: 90,
      name: 'Windows 7 Professional N SP1 COEM',
      disabled: true // doesn't work anymore
    }, {
      id: 91,
      name: 'Windows 7 Professional SP1 COEM',
      disabled: true // doesn't work anymore
    }, {
      id: 92,
      name: 'Windows 7 Starter SP1 COEM',
      disabled: true // doesn't work anymore
    }, {
      id: 93,
      name: 'Windows 7 Ultimate K SP1 COEM',
      disabled: true // doesn't work anymore
    }, {
      id: 94,
      name: 'Windows 7 Ultimate KN SP1 COEM',
      disabled: true // doesn't work anymore
    }, {
      id: 95,
      name: 'Windows 7 Ultimate N SP1 COEM',
      disabled: true // doesn't work anymore
    }, {
      id: 96,
      name: 'Windows 7 Ultimate SP1 COEM',
      disabled: true // doesn't work anymore
    }, {
      id: 97,
      name: 'Windows 7 Home Premium KN SP1 COEM',
      disabled: true // doesn't work anymore
    }, {
      id: 98,
      name: 'Windows 7 Professional KN SP1 COEM',
      disabled: true // doesn't work anymore
    }]
  }, {
    name: 'Windows 8.1',
    products: [/*{
      id: 45,
      name: '45' // doesn't work anymore
    }, {
      id: 46,
      name: '46' // doesn't work anymore
    }, {
      id: 47,
      name: '47' // doesn't work anymore
    }, {
      id: 48,
      name: 'Windows 8.1 Single Language' // doesn't work anymore
    }, {
      id: 49,
      name: '49' // doesn't work anymore
    }, {
      id: 50,
      name: '50' // doesn't work anymore
    }, {
      id: 51,
      name: '51' // doesn't work anymore
    }*/, {
      id: 52,
      name: 'Windows 8.1'
    }/*, {
      id: 53,
      name: '53' // doesn't work anymore
    }, {
      id: 54,
      name: '54' // doesn't work anymore
    }*/, {
      id: 55,
      name: 'Windows 8.1 N'
    }/*, {
      id: 56,
      name: '56' // doesn't work anymore
    }, {
      id: 57,
      name: '57' // doesn't work anymore
    }, {
      id: 58,
      name: '58' // doesn't work anymore
    }, {
      id: 59,
      name: '59' // doesn't work anymore
    }, {
      id: 60,
      name: '60' // doesn't work anymore
    }*/, {
      id: 61,
      name: 'Windows 8.1 K'
    }/*, {
      id: 62,
      name: '62' // MSError
    }, {
      id: 63,
      name: '63' // doesn't work anymore
    }, {
      id: 64,
      name: '64' // doesn't work anymore
    }, {
      id: 65,
      name: '65' // doesn't work anymore
    }, {
      id: 66,
      name: '66' // doesn't work anymore
    }, {
      id: 67,
      name: '67' // doesn't work anymore
    }, {
      id: 68,
      name: 'Windows 8.1 Professional LE' // doesn't work anymore
    }, {
      id: 69,
      name: 'Windows 8.1 Professional LE K' // doesn't work anymore
    }, {
      id: 70,
      name: 'Windows 8.1 Professional LE KN' // doesn't work anymore
    }, {
      id: 71,
      name: 'Windows 8.1 Professional LE N' // doesn't work anymore
    }, {
      id: 72,
      name: '72' // doesn't work anymore
    }, {
      id: 73,
      name: '73' // doesn't work anymore
    }, {
      id: 74,
      name: '74' // doesn't work anymore
    }*/]
  }, {
    name: 'Windows 10',
    products: [/*{
      id: 75,
      name: 'Windows 10 Education (Academic) - build 10240' // doesn't work anymore
    }, {
      id: 76,
      name: 'Windows 10 Education KN (Academic) - build 10240'
    }, {
      id: 77,
      name: 'Windows 10 Education N (Academic) - build 10240'
    }, {
      id: 78,
      name: 'Windows 10 China Get Genuine - build 10240'
    }, {
      id: 79,
      name: 'Windows 10 Pro-Home - build 10240'
    }, {
      id: 80,
      name: 'Windows 10 ProKN-HomeKN - build 10240'
    }, {
      id: 81,
      name: 'Windows 10 ProN-HomeN - build 10240'
    }, */{
      id: 82,
      name: 'Windows 10 Single Language'
    }/*, {
      id: 83,
      name: '83'
    }, {
      id: 84,
      name: '84' # License Key required
    }, {
      id: 85,
      name: '85'
    }, {
      id: 86,
      name: '86'
    }, {
      id: 87,
      name: '87'
    }, {
      id: 88,
      name: '88'
    }, {
      id: 89,
      name: '89'
    }, {
      id: 90,
      name: '90'
    }*/, {
      id: 99,
      name: 'Windows 10 Pro-Home - Version 1511'
    }, {
      id: 100,
      name: 'Windows 10 Education - Version 1511'
    }, {
      id: 101,
      name: 'Windows 10 Education KN - Version 1511'
    }, {
      id: 102,
      name: 'Windows 10 Education N - Version 1511'
    }, {
      id: 103,
      name: 'Windows 10 China Get Genuine - Version 1511'
    }, {
      id: 104,
      name: 'Windows 10 ProKN-HomeKN - Version 1511'
    }, {
      id: 105,
      name: 'Windows 10 ProN-HomeN - Version 1511'
    }, {
      id: 106,
      name: 'Windows 10 Single Language - Version 1511'
    }, {
      id: 1203,
      name: 'Windows 10 October 2018 Update'
    }, {
      id: 1384,
      name: 'Windows 10 May 2019 Update'
    }/*, {
      id: 1621,
      name: '1621'
    }, {
      id: 1622,
      name: '1622'
    }, {
      id: 1623,
      name: '1623'
    }, {
      id: 1624,
      name: '1624'
    }*/, {
      id: 1625,
      name: 'Windows 10 Education'
    }, {
      id: 1626,
      name: 'Windows 10 May 2020 Update'
    }, {
      id: 1627,
      name: 'Windows 10 Home China Chinese (Simplified)'
    }/*, {
      id: 1628,
      name: '1628'
    }, {
      id: 1629,
      name: '1629'
    }, {
      id: 1630,
      name: '1630'
    }*/]
  }]

  html += templates.select

  editions.forEach(edition => {
    let list

    edition.products.forEach(product => {
      list += templates.option.replace('%value%', product.id).replace('%name%', product.name ? product.name : 'N/A').replace('%disabled%', product.disabled ? 'disabled' : '')
    })

    html += templates.optgroup.replace('%label%', edition.name).replace('%list%', list)
  })

  document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('product-edition').innerHTML = html
  })

  /*<option value="107">Windows 10 IoT Core Insider Preview - Version 1511</option>\
  <option value="108">Windows 10 IoT Core Insider Preview - Build 14262</option>\
  <option value="109">Windows 10 Pro-Home - Version 1511 (Updated Feb 2016)</option>\
  <option value="110">Windows 10 Education - Version 1511 (Updated Feb 2016)</option>\
  <option value="111">Windows 10 Education KN - Version 1511 (Updated Feb 2016)</option>\
  <option value="112">Windows 10 Education N - Version 1511 (Updated Feb 2016)</option>\
  <option value="113">Windows 10 China Get Genuine - Version 1511 (Updated Feb 2016)</option>\
  <option value="114">Windows 10 ProKN-HomeKN - Version 1511 (Updated Feb 2016)</option>\
  <option value="115">Windows 10 ProN-HomeN - Version 1511 (Updated Feb 2016)</option>\
  <option value="116">Windows 10 Single Language - Version 1511 (Updated Feb 2016)</option>\
  <option value="117">Windows 10 IoT Core Insider Preview - Build 14279</option>\
  <option value="118">Windows 10 China Only Insider Preview - Build 14295</option>\
  <option value="119">Windows 10 Client Insider Preview - Build 14295</option>\
  <option value="120">Windows 10 Core Single Language Insider Preview - Build 14295</option>\
  <option value="121">Windows 10 Education Insider Preview - Build 14295</option>\
  <option value="122">Windows 10 Enterprise VL Insider Preview - Build 14295</option>\
  <option value="244">Windows 10</option>\
  <option value="245">Windows 10 N</option>\
  <option value="246">Windows 10 Single Language</option>\
  <option value="247">Windows 10 China Get Genuine</option>\
  <option value="248">?</option>\
  <option value="249">?</option>\
  <option value="250">?</option>';*/
})()
