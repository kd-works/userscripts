// ==UserScript==
// @name        Vercel
// @namespace   works.kasai.userscripts.vercel
// @version     1.1.3
// @author      Kasai. (kasai.moe)
// @description Remove useless component
// @homepage    https://userscripts.kasai.works#vercel
// @icon        https://assets.vercel.com/image/upload/q_auto/front/favicon/vercel/favicon.ico
// @updateURL   https://gitlab.com/kd-works/userscripts/raw/main/Vercel.user.js
// @downloadURL https://gitlab.com/kd-works/userscripts/raw/main/Vercel.user.js
// @supportURL  https://gitlab.com/kd-works/userscripts/issues/new
// @match       https://vercel.com/*
// @run-at      document-body
// @grant       GM_addStyle
// @noframes
// ==/UserScript==

(() => {
  'use strict'

  GM_addStyle('.geist-wrapper>.geist-container div[role="menu"] .popover>.menu>.geist-container{overflow:hidden!important}')
})()
