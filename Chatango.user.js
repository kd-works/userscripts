// ==UserScript==
// @name        Chatango
// @namespace   works.kasai.userscripts.chatango
// @version     0.3.7
// @author      Kasai. (kasai.moe)
// @description Adapt the css to the size of the page.
// @homepage    https://userscripts.kasai.works#chatango
// @icon        https://www.chatango.com/favicon.ico
// @updateURL   https://gitlab.com/kd-works/userscripts/raw/main/Chatango.user.js
// @downloadURL https://gitlab.com/kd-works/userscripts/raw/main/Chatango.user.js
// @supportURL  https://gitlab.com/kd-works/userscripts/issues/new
// @match       http*://*.chatango.com
// @match       http*://*.chatango.com/*
// @run-at      document-start
// @grant       GM_addStyle
// @noframes
// ==/UserScript==

(() => {
  'use strict'

  GM_addStyle('body{overflow:hidden!important}')
})()
